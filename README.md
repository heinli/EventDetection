# EventDetection
## Structure
All Python scripts go into the src_python folder.

All Matlab scripts go into the src_matlab folder.

All *.hdf5 files go into the data folder.

*.hdf5 files can be downloaded at https://mediatum.ub.tum.de/1375836

Documentation can be found in the latex-folder.