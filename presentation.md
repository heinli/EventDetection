# Präsentation

## Chapters
1. Outline given Problem
2. General Structure of solutions
3. Types of events
4. Wavelet-Transform
5. Hilbert-Huang-Transform

## Outline given Problem
- Event Detection from Existing Datasets using Open Source Techniques and Software
- Detect Low-Powered Signals
- Extract Event-Features
- Build (start-up) feature database

## Types of Events
- Power-Consumption
- Macro-Events (Events, whose signature last for several periods)
- Micro-Events (Events, whose signature is around 1/10th of a period)

## General Structure of Solutions
1. Preprocess data (apply filters)
2. Decompose data
3. Detect Events from the decomposed data
4. Extract features from time around Event

## What is a transformation?
A transformation is the process of taking a signal and mapping/matching it to several independant base-signals to gain knowledge about the original signal.
In mathematics the different base signals would be called orthogonal to one another.
The most basic transformation is the Fourier Transform, where those base signals are sine and cosine waves.
But what if wanted to use base signals that represent your features better than a sine wave?
&rarr; Wavelet Transform
Instead of a Sine Wave we use a so-called Wavelet as a base function.
Sine Waves go on forever, which means that local changes cannot be detected when matching a signal to a sine wave.
Wavelets are locally restricted, which means that a good matching to a Wavelet positioned at time T will tell you *where* the match happend. Namely at time T.
You can also strech the base-wavelet to gain a new, wider base signal. Then a match with a streched base signal will tell you that there was a match at time T with a wide (low frequency) base signal.

## Wavelet-Transform
### Process
1. Preprocess data: Apply a high-pass filter
2. Apply wavelet transform
3. Extract peaks from resulting diagram &rarr; event
4. Copy chunks around the detected event\
### Pros and cons
+ (+) Very fast
+ (+) very good at detecting Macro-Events
+ (+) not many parameters to optimize
+ (-) not optimal for detecting Micro-Events
+ (-) no features for event-classification can be extracted
+ (-) the event-length can't be determined

## Hilbert-Huang-Transform
1. ? High-pass filter
2. Empirical mode decomposition
3. Hilbert-Transform &rarr; analytic signal
4. Detect events from peaks in the amplitude of analytic signal
5. Copy the chunk with the detected event
### Pros and cons
+ (+) very good at detecting micro-events
+ (+) can give more features to be used for event-classification (instantaneous frequency)
+ (+) event-length can easily be extracted
+ (-) not very fast