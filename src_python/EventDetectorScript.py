from EventDetectorGlobals import *
import EventDetectorHHT
import multiprocessing
import h5py
import sys
import time
import traceback
import csv

INITIAL_OFFSET = 0
CHUNK_SIZE = 250000  # each chunk is 1 second

SIGNAL = 'current'
PHASE = '1'
USED_EMDs = [0, 1, 2]
PEAK_SETTINGS = event_settings(height=500, prominence=800, width=10, distance=10, rel_height=0.9)


def main():
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = "../data/clear-2017-06-21T06-07-20.122303T+0200-0028508.hdf5"

    f = h5py.File(filename, "r")

    # read data to operate on
    original_data_complete = f[SIGNAL + PHASE][0:len(f[SIGNAL + PHASE])]
    # original_data = original_data_complete[0:400000]  # 4 seconds for testing purposes
    original_data = original_data_complete
    f.close()

    start_time = time.time()
    # all_events = process_sequentially(original_data)
    all_events = process_parallel(original_data)
    processing_time = time.time() - start_time
    print 'Processing took ' + str(int(processing_time / 60)) + ' min and ' + str(processing_time % 60) + ' s'

    path = filename.split('/')
    pure_filename = path[len(path) - 1].split('.')
    target_filename = "../data/"
    for i in range(0, len(pure_filename) - 1):
        target_filename += pure_filename[i] + '.'

    target_filename += 'DetectedEvents.csv'
    write_csv(target_filename, all_events)

    print 'Results written to file. Program End'


def process_sequentially(all_data):
    all_events = []
    current_offset = INITIAL_OFFSET

    n_chunks = all_data.size / CHUNK_SIZE
    print 'We have to process ' + str(n_chunks) + ' Chunks with ' + str(CHUNK_SIZE) + ' Data points in each Chunk'
    for i in range(0, n_chunks):
        # peakProperties and smoothed currently only used for graphical display of results
        chunk_result = EventDetectorHHT.process_chunk(
            InputChunk(Offset=current_offset, CurrentData=all_data[current_offset:current_offset + CHUNK_SIZE]), used_emds=USED_EMDs, settings=PEAK_SETTINGS, graphical=False)

        all_events.append(chunk_result.Events)
        # todo use overlapping window
        current_offset += CHUNK_SIZE
        print 'Chunk ' + str(i + 1) + '/' + str(n_chunks) + ' done'

    all_events = [item for sublist in all_events for item in sublist]
    return all_events


def parallel_wrapper(input_chunk):
    try:
        return EventDetectorHHT.process_chunk(input_chunk, used_emds=USED_EMDs, settings=PEAK_SETTINGS, graphical=False)
    except Exception as e:
        print 'Exception in worker Process'
        traceback.print_exc()
        raise e


def process_parallel(all_data):
    n_processes = multiprocessing.cpu_count()
    print 'Creating pool with %d processes\n' % n_processes
    pool = multiprocessing.Pool(n_processes)

    current_offset = INITIAL_OFFSET

    n_chunks = all_data.size / CHUNK_SIZE
    data_chunks = []
    for i in range(0, n_chunks):
        data_chunks.append(
            InputChunk(Offset=current_offset, CurrentData=all_data[current_offset:current_offset + CHUNK_SIZE]))
        current_offset += CHUNK_SIZE

    print 'We have to process ' + str(n_chunks) + ' Chunks with ' + str(CHUNK_SIZE) + ' Data points in each Chunk'
    print 'Using ' + str(n_processes) + ' Processes to do this!'
    all_chunk_results = pool.map(parallel_wrapper, data_chunks)

    all_events = []
    for chunkResult in all_chunk_results:
        event_list = chunkResult.Events
        for event in event_list:
            all_events.append(event)
    return all_events


def write_csv(filename, events):
    with open(filename, 'wb')as result_file:
        csv_writer = csv.writer(result_file, delimiter=';')
        counter = 0
        csv_writer.writerow(['Event#', 'Start_Tick', 'End_Tick', 'Duration', 'Overlap', 'Type'])
        for i, event in enumerate(events):
            csv_writer.writerow([i, event.Start_Tick, event.End_Tick, event.Duration, event.Overlap, event.Type])
            counter += 1


if __name__ == "__main__":
    main()
