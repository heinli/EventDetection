# coding=utf-8
import bisect
import threading

from matplotlib.widgets import Button, TextBox, CheckButtons

from EventDetectorGlobals import *
import EventDetectorHHT
import matplotlib.pyplot as plt
import numpy as np
import sys
import h5py
from matplotlib import patches

OFFSET = 3000
AMOUNT = 50000
ACCURACY = 1
MILLIS_PER_TICK = 4

SIGNAL = 'current'
PHASE = '1'

ALL_EVENTS = None
ORIGINAL_DATA_COMPLETE = None
PEAK_SETTINGS = event_settings(height=500, prominence=800, width=10, distance=10, rel_height=0.9)
EMD_SETTINGS = [0, 1, 2]
EMD_AMT = 5
BUTTONS = None


class Display(object):
    display_figure = None
    graph1 = None
    graph2 = None
    graph3 = None
    x_axis = None
    offset = None
    amount = None
    original_data = None
    summed_emds = None
    used_emds = None
    single_emds = None
    events = None
    to_redraw = {}

    def __init__(self, offset, amount, original_data, summed_emds, single_emds, used_emds, events):
        global MILLIS_PER_TICK
        global ACCURACY
        self.display_figure = plt.figure(figsize=(7, 4.5))
        self.offset = offset
        self.amount = amount
        self.original_data = original_data
        self.summed_emds = summed_emds
        self.single_emds = single_emds
        self.used_emds = used_emds
        self.events = events
        self.x_axis = np.arange(offset * MILLIS_PER_TICK, (offset + amount) * MILLIS_PER_TICK,
                                ACCURACY * MILLIS_PER_TICK)
        self.graph1 = self.display_figure.add_subplot(311)
        self.graph2 = self.display_figure.add_subplot(312, sharex=self.graph1)
        self.graph3 = self.display_figure.add_subplot(313, sharex=self.graph1)
        self.to_redraw["raw graph"] = True
        self.to_redraw["boxes"] = True
        self.to_redraw["emd"] = True
        self.to_redraw["sum and peaks"] = True
        self.redraw_all()

    def redraw_all(self):
        self.redraw_raw_graph()
        self.redraw_boxes()
        self.redraw_emd_graph()
        self.redraw_sum_and_peaks()
        
        self.display_figure.show()
        self.display_figure.canvas.toolbar.update()

    def redraw_raw_graph(self):
        if self.to_redraw["raw graph"]:
            self.to_redraw["raw graph"] = False
            self.graph1.clear()
            self.graph2.clear()
            self.graph1.set_xlabel(u"time (\u00b5s)")
            self.graph1.set_ylabel(SIGNAL + " " + PHASE)
            self.graph1.plot(self.x_axis, self.original_data, label='original data')
            self.graph1.autoscale(tight=True,axis='x')
            
    def redraw_sum_and_peaks(self):
        if self.to_redraw["sum and peaks"]:
            self.to_redraw["sum and peaks"] = False
            self.graph3.clear()
            self.graph3.set_xlabel(u"time (\u00b5s)")
            labelText = "summ ("
            for emd_nr in self.used_emds:
                labelText = labelText + "IMF" + str(emd_nr) + " +"
            labelText = labelText[:-2]
            labelText = labelText + ")"
            
            self.graph3.plot(self.x_axis, self.summed_emds, label=labelText)
            
            for event in self.events:
               if event.Peak_Info is not None:
                   pInfo = event.Peak_Info
                   self.graph3.plot(event.Max_Pos * MILLIS_PER_TICK, self.summed_emds[event.Max_Pos - self.offset], "x", color="y")
                   self.graph3.vlines(x=event.Max_Pos * MILLIS_PER_TICK, ymin=self.summed_emds[event.Max_Pos - self.offset] - pInfo.Prominence, ymax=self.summed_emds[event.Max_Pos - self.offset], color="y")
                   self.graph3.hlines(y=pInfo.Width_Height, xmin=event.Start_Tick * MILLIS_PER_TICK, xmax=event.End_Tick * MILLIS_PER_TICK, color="y")
            
            self.graph3.legend()
            self.graph3.autoscale(tight=True,axis='x')
            
    def redraw_boxes(self):
        if self.to_redraw["boxes"]:
            self.to_redraw["boxes"] = False
            for i, event in enumerate(self.events):
                left = event.Start_Tick
                right = event.End_Tick
                width = right - left
                search_range = self.original_data[int(left) - self.offset:int(right) - self.offset]
                if search_range.size < 1:
                    continue
                bottom = min(search_range)
                top = max(search_range)
                height = top - bottom
                if event.Type == SUB_MICROEVENT:
                    color = 'r'
                else:
                    color = 'g'
                self.graph1.add_patch(
                    patches.Rectangle((left * MILLIS_PER_TICK, bottom), width * MILLIS_PER_TICK, height, linewidth=1,
                                      edgecolor=color, facecolor='none'))

    def redraw_emd_graph(self):
        if self.to_redraw["emd"]:
            self.to_redraw["emd"] = False
            self.graph2.clear()
            self.graph2.set_xlabel(u"time (\u00b5s)")
            for i, EMDAmp in self.single_emds:
                self.graph2.plot(self.x_axis, EMDAmp, label="IMF " + str(i))
            self.graph2.legend()
            self.graph2.autoscale(tight=True,axis='x')
        pass

    def set_original_data(self, original_data, offset, amount):
        self.original_data = original_data
        self.offset = offset
        self.amount = amount
        self.x_axis = np.arange(offset * MILLIS_PER_TICK, (offset + amount) * MILLIS_PER_TICK,
                                ACCURACY * MILLIS_PER_TICK)
        self.to_redraw["raw graph"] = True

    def set_summed_emds(self, summed_emds):
        self.summed_emds = summed_emds
        self.to_redraw["sum and peaks"] = True
        
    def set_used_emds(self, used_emds):
        self.used_emds = used_emds
        self.to_redraw["sum and peaks"] = True

    def set_single_emds(self, single_emds):
        self.single_emds = single_emds
        self.to_redraw["emd"] = True

    def set_events(self, events):
        self.events = events
        self.to_redraw["raw graph"] = True
        self.to_redraw["boxes"] = True
        self.to_redraw["sum and peaks"] = True


class Controller(object):
    all_data = None
    amount = None
    offset = None
    all_events = None
    display = None
    menu = None
    menu_elements = []
    used_emds = None
    emd_amt = None
    emds = None
    settings = PEAK_SETTINGS
    redo_from_scratch = False

    window = None
    check_list = None
    check_list_axes = None

    def __init__(self, all_data, amount, offset):
        self.amount = amount
        self.offset = offset
        self.all_data = all_data
        self.emd_amt = EMD_AMT
        self.used_emds = EMD_SETTINGS
        used_data = all_data[OFFSET:OFFSET + AMOUNT]
        self.all_events = EventDetectorHHT.process_chunk(InputChunk(Offset=offset, CurrentData=used_data),
                                                         used_emds=self.used_emds, emd_amt=self.emd_amt, settings=self.settings)
        self.display = Display(offset, amount, used_data, self.all_events.Summ, enumerate(self.all_events.EMDAmps), self.used_emds,
                               self.all_events.Events)
        self.window = plt.figure(figsize=(4, 6))
        self.setup_menu()
        plt.show()

    def setup_menu(self):
        ax_btn = self.window.add_axes([0.75, 0.08, 0.25, 0.05])
        apply_button = Button(ax_btn, 'apply')
        apply_button.on_clicked(self.apply_changes)
        self.menu_elements.append(apply_button)
        
        ax_btn2 = self.window.add_axes([0.75, 0.0, 0.25, 0.05])
        add_button = Button(ax_btn2, 'More Graphs')
        add_button.on_clicked(self.showAdditionalGraphs)
        self.menu_elements.append(add_button)

        ax_field = self.window.add_axes([0.2, 0.95, 0.5, 0.05])
        text_box = TextBox(ax_field, "amount", initial=str(self.amount))
        text_box.on_submit(self.set_amount)
        self.menu_elements.append(text_box)

        ax_field = self.window.add_axes([0.2, 0.9, 0.5, 0.05])
        text_box = TextBox(ax_field, "offset", initial=str(self.offset))
        text_box.on_submit(self.set_offset)
        self.menu_elements.append(text_box)

        ax_field = self.window.add_axes([0.2, 0.8, 0.5, 0.05])
        text_box = TextBox(ax_field, "height", initial=str(self.settings.height))
        text_box.on_submit(self.set_height)
        self.menu_elements.append(text_box)

        ax_field = self.window.add_axes([0.2, 0.75, 0.5, 0.05])
        text_box = TextBox(ax_field, "prominence", initial=str(self.settings.prominence))
        text_box.on_submit(self.set_prominence)
        self.menu_elements.append(text_box)

        ax_field = self.window.add_axes([0.2, 0.7, 0.5, 0.05])
        text_box = TextBox(ax_field, "width", initial=str(self.settings.width))
        text_box.on_submit(self.set_width)
        self.menu_elements.append(text_box)

        ax_field = self.window.add_axes([0.2, 0.65, 0.5, 0.05])
        text_box = TextBox(ax_field, "distance", initial=str(self.settings.distance))
        text_box.on_submit(self.set_distance)
        self.menu_elements.append(text_box)

        ax_field = self.window.add_axes([0.2, 0.60, 0.5, 0.05])
        text_box = TextBox(ax_field, "rel_height", initial=str(self.settings.rel_height))
        text_box.on_submit(self.set_rel_height)
        self.menu_elements.append(text_box)

        ax_field = self.window.add_axes([0.2, 0.50, 0.5, 0.05])
        text_box = TextBox(ax_field, "emd_amt", initial=str(max(self.used_emds) + 1))
        text_box.on_submit(self.set_emd_amt)
        self.menu_elements.append(text_box)

        self.check_list_axes = self.window.add_axes([0.2, 0.0, 0.5, 0.4])
        # self.check_list = CheckButtons(self.check_list_axes, list(map(lambda x: "EMD" + str(x), range(self.emd_amt))),
        #                               list(map(lambda x: True, range(self.emd_amt))))
        # self.check_list.on_clicked(self.set_used_emds)
        self.setup_check_list()

    def setup_check_list(self):
        self.check_list = CheckButtons(self.check_list_axes, list(map(lambda x: "IMF" + str(x), range(self.emd_amt))),
                                       list(map(lambda x: self.used_emds.count(x) > 0, range(self.emd_amt))))
        self.check_list.on_clicked(self.set_used_emds)
        self.window.show()

    def set_amount(self, text):
        try:
            self.amount = int(text)
            self.redo_from_scratch = True
        except ValueError:
            print "amount is invalid"

    def set_offset(self, text):
        try:
            self.offset = int(text)
            self.redo_from_scratch = True
        except ValueError:
            print "offset is invalid"

    def set_height(self, text):
        try:
            self.settings = event_settings(height=int(text),
                                           prominence=self.settings.prominence,
                                           width=self.settings.width,
                                           distance=self.settings.distance,
                                           rel_height=self.settings.rel_height)
            self.apply_changes("")
        except ValueError:
            print "height is invalid"

    def set_prominence(self, text):
        try:
            self.settings = event_settings(height=self.settings.height,
                                           prominence=int(text),
                                           width=self.settings.width,
                                           distance=self.settings.distance,
                                           rel_height=self.settings.rel_height)
            self.apply_changes("")
        except ValueError:
            print "prominence is invalid"

    def set_width(self, text):
        try:
            self.settings = event_settings(height=self.settings.height,
                                           prominence=self.settings.prominence,
                                           width=int(text),
                                           distance=self.settings.distance,
                                           rel_height=self.settings.rel_height)
            self.apply_changes("")
        except ValueError:
            print "width is invalid"

    def set_distance(self, text):
        try:
            self.settings = event_settings(height=self.settings.height,
                                           prominence=self.settings.prominence,
                                           width=self.settings.width,
                                           distance=int(text),
                                           rel_height=self.settings.rel_height)
            self.apply_changes("")
        except ValueError:
            print "distance is invalid"

    def set_rel_height(self, text):
        try:
            self.settings = event_settings(height=self.settings.height,
                                           prominence=self.settings.prominence,
                                           width=self.settings.width,
                                           distance=self.settings.distance,
                                           rel_height=float(text))
            self.apply_changes("")
        except ValueError:
            print "rel_height is invalid"

    def set_emd_amt(self, text):
        try:
            if self.emd_amt == int(text):
                return
            if self.emd_amt > int(text):
                self.used_emds = [i for i in self.used_emds if i < int(text)]
            else:
                self.used_emds = self.used_emds + (list(range(self.emd_amt, int(text))))
            self.emd_amt = int(text)
            # self.used_emds = list(range(self.emd_amt))
            plt.sca(self.check_list_axes)
            plt.cla()
            self.setup_check_list()
            self.apply_changes("")
        except ValueError:
            print "emd_amt is wrong"

    def set_used_emds(self, text):
        for i in range(self.emd_amt):
            if "IMF" + str(i) == text:
                try:
                    self.used_emds.remove(i)
                except ValueError:
                    bisect.insort(self.used_emds, i)
                break
        self.apply_changes("")

    def apply_changes(self, forget):
        used_data = self.all_data[self.offset:self.offset + self.amount]
        if self.redo_from_scratch:  # only happens when offset and amount have been changed
            self.all_events = EventDetectorHHT.process_chunk(InputChunk(Offset=self.offset, CurrentData=used_data),
                                                             used_emds=self.used_emds, emd_amt=self.emd_amt,
                                                             settings=self.settings)
            self.display.set_original_data(used_data, offset=self.offset, amount=self.amount)
            self.redo_from_scratch = False
        else:  # still using the same data set => chosen EMDs and/or peak-detection-parameters changed
            self.all_events = EventDetectorHHT.reprocess_chunk(InputChunk(Offset=self.offset, CurrentData=used_data),
                                                               self.all_events, self.offset, self.settings,
                                                               self.used_emds, emd_amt=self.emd_amt)
        self.display.set_summed_emds(self.all_events.Summ)
        self.display.set_events(self.all_events.Events)
        self.display.set_single_emds(map(lambda i: (i, self.all_events.EMDAmps[i]), range(0, self.emd_amt)))
        # self.display.set_single_emds(self.all_events.EMDAmps)
        self.display.set_used_emds(self.used_emds)
        self.display.redraw_all()
        
    def showAdditionalGraphs(self, text):
        x_axis = np.arange(self.offset * MILLIS_PER_TICK, (self.offset + self.amount) * MILLIS_PER_TICK,
                                ACCURACY * MILLIS_PER_TICK)
        imf_figure = plt.figure(figsize=(7, 4.5))
        emds = self.all_events.EMDs
        n_emds = emds.shape[0]
        
        graph1 = imf_figure.add_subplot(100 * n_emds + 11)
        graph1.plot(x_axis, emds[0], linewidth=1, color='r')
        graph1.set_ylabel("IMF 0")
        currentGraph = []
        for i in range(1, n_emds):
            currentGraph = imf_figure.add_subplot(100 * n_emds + 10 + i + 1, sharex=graph1)
            currentGraph.plot(x_axis, emds[i], linewidth=1, color='r')
            currentGraph.set_ylabel("IMF " + str(i))
        
        currentGraph.set_ylabel("Residue")
        imf_figure.show()
        
        # little test to demonstrate that the sum of all imfs gives the original signal
        '''testfigure = plt.figure(figsize=(7, 4.5))
        sum = np.zeros((emds.shape[1]))
        for i in range(0, n_emds):
            sum = sum + emds[i]
        
        graph = testfigure.add_subplot(111)
        graph.plot(x_axis, sum)      
        testfigure.show()'''


def main():
    global ORIGINAL_DATA_COMPLETE
    global ALL_EVENTS
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        print "opening: " + filename
    else:
        filename = "../data/clear-2017-06-21T06-07-20.122303T+0200-0028508.hdf5"

    f = h5py.File(filename, "r")

    # read data to operate on
    ORIGINAL_DATA_COMPLETE = f[SIGNAL + PHASE][0:len(f[SIGNAL + PHASE])]
    original_data = ORIGINAL_DATA_COMPLETE[OFFSET:OFFSET + AMOUNT]
    f.close()

    thread = threading.Thread(target=Controller, args=[ORIGINAL_DATA_COMPLETE, AMOUNT, OFFSET])
    thread.start()
    # calculate the results
    chunk_result = EventDetectorHHT.process_chunk(InputChunk(Offset=OFFSET, CurrentData=original_data))
    events = chunk_result.Events
    for i, event in enumerate(events):
        print event.Type
        print event.Start_Tick
        print event.End_Tick
        print " "
    thread.join()


if __name__ == "__main__":
    main()
