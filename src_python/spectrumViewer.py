import h5py
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal as sig

# set parameters to specify wanted output
phase = '1'
offset = 0
amt = 10000

sampling_freq_in_Hz = 250000.
filter_low_freq = True

# choose file
f = h5py.File("../data/FILENAME.hdf5", "r")

x = np.arange(0, amt, 1)

odata = f['current' + phase][offset:offset + amt]  # read relevant data

if filter_low_freq:  # filter if necessary
    b, a = sig.butter(5, 0.03, btype='high', analog=False)
    data = sig.filtfilt(b, a, odata)
else:
    data = odata

# calculate and display fft
spec = np.fft.fft(data)
spec = np.abs(spec)
spec = 20 * np.log10(spec)
x = np.arange(0, sampling_freq_in_Hz / 2, sampling_freq_in_Hz / amt)
plt.plot(x, spec[0:amt / 2])

plt.xlabel('freq (Hz)')
plt.ylabel('signal-strength in dB')
plt.show()

f.close()
