import h5py
import numpy as np
import matplotlib.pyplot as plt

# choose file
f = h5py.File("D:\Workspace\Python\EventDetection\data\clear-2017-06-28T15-29-49.319945T+0200-0033803.hdf5", "r")

# set parameters to specify wanted output
phase = '1'
offset = 0
amt = 5000
reduction_factor = 1

plot_sums = False

v_to_be_graphed = [
    'voltage' + '1',
    #   'voltage' + '2',
    #   'voltage' + '3'
]

i_to_be_graphed = [
    'current' + '1',
    # 'current' + '2',
    # 'current' + '3'
]

# constants
TIMESTEP = 4 / 1000.


# create output
# create x-labels
x = np.arange(offset * TIMESTEP, (offset + amt * reduction_factor) * TIMESTEP,
              reduction_factor * TIMESTEP)
fig, ax1 = plt.subplots()

color = 'tab:red'

# label x-axes
ax1.set_xlabel(u"time (ms)")
ax1.set_ylabel('voltage', color=color)
ax1.tick_params(axis='y')

# plot voltages
for v in v_to_be_graphed:
    if v == "voltage1":  # treat current 1 differently, because we know the correction-factor for it
        ax1.plot(x, f[v][
                    offset:offset + amt * reduction_factor:reduction_factor] * 0.011170775,
                 label=v)
    else:
        ax1.plot(x, f[v][offset:offset + amt * reduction_factor:reduction_factor], label=v)
# plot sum of all voltages
if plot_sums:
    ax1.plot(x, f['voltage1'][offset:offset + amt:1] + f['voltage2'][offset:offset + amt:1] + f['voltage3'][
                                                                                              offset:offset + amt:1],
             label='all')

ax1.legend()

ax2 = ax1.twinx()

color = 'tab:blue'
ax2.set_ylabel('current', color=color)
ax2.tick_params(axis='y', color=color)

# plot currents
for i in i_to_be_graphed:
    if i == "current1":  # treat current 1 differently, because we know the correction-factor for it
        ax2.plot(x,
                 f[i][offset:offset + amt * reduction_factor:reduction_factor] * 0.0009625,
                 label=i)
    else:
        ax2.plot(x, f[i][offset:offset + amt * reduction_factor:reduction_factor], label=i)
# plot sum of all currents
if plot_sums:
    ax2.plot(x, f['current1'][offset:offset + amt:1] + f['current2'][offset:offset + amt:1] + f['current3'][
                                                                                              offset:offset + amt:1],
             label='all')

fig.tight_layout()
ax2.legend()
plt.show()

f.close()
