import h5py
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import scipy.signal as sig

from src_python import EventDetectorHHT, EventDetectorGlobals


amt = 5000
offset = 0

DEMO_KIND = "sine"
# DEMO_KIND = "chirp"
# DEMO_KIND = "imf"
# DEMO_KIND = "raw_data"
# DEMO_KIND = "wavelet"

#choose file
f = h5py.File("D:\Workspace\Python\EventDetection\data\clear-2017-06-06T00-25-08.095950T+0200-0017591.hdf5", "r")

#setup display
fig = plt.figure()
ax = fig.add_subplot(211, projection='3d')

zs = np.arange(offset, offset + amt)
ys = f['current1'][offset:offset + amt]
f.close()

N_ITERATIONS = 30
X_FOR_SPLINE = np.arange(offset, offset + amt)
EventDetectorHHT.X_FOR_SPLINE = X_FOR_SPLINE


def calc_emds(input_chunk, emd_amt):
    global X_FOR_SPLINE
    global N_ITERATIONS

    residue = input_chunk.CurrentData
    chunk_size = residue.size
    # n_emd = max(max(wanted_emds) + 1, emd_amt)
    n_emd = emd_amt

    emd_list = np.zeros((n_emd, chunk_size))
    if X_FOR_SPLINE is None or X_FOR_SPLINE.size != chunk_size:
        X_FOR_SPLINE = np.arange(0, chunk_size, 1)

    for j in range(0, n_emd):  # how many EMDs to be calculated
        emd = residue  # current (temporary) emd is the last iterations residue
        stoppage_memory = None
        for i in range(0, N_ITERATIONS):  # chunk_size of smoothing-iterations to be executed
            # calculate all minimums and maximums

            max_spline = EventDetectorHHT.get_envelope(emd, True)
            min_spline = EventDetectorHHT.get_envelope(emd, False)
            # subtract the average of the splines from the current residue to get better approximation of EMD
            avg = (max_spline + min_spline) / 2
            new_emd = emd - avg
            should_stop, stoppage_memory = EventDetectorHHT.standard_dev_stoppage(emd, stoppage_memory)
            should_stop = should_stop or EventDetectorHHT.threshold_stoppage(min_spline, max_spline, avg)
            emd = new_emd
            if should_stop:
                break

        # print inner counter
        print 'Emd ' + str(j) + " (" + str(i) + ")"
        # update residue by removing new EMD from the signal
        residue = residue - emd
        # add new EMD to list
        emd_list[j, :] = emd

    return emd_list


if DEMO_KIND == "imf":
    zs = X_FOR_SPLINE
    ys = calc_emds(input_chunk=EventDetectorGlobals.InputChunk(Offset=offset, CurrentData=ys), emd_amt=3)[1]
if DEMO_KIND == "sine":
    zs = np.arange(0, 6 * np.pi, 0.01)
    ys = np.sin(zs)
if DEMO_KIND == "chirp":
    zs = np.arange(0, 6 * np.pi, 0.01)
    ys = sig.chirp(zs, 0.1, 6 * np.pi, 1)
if DEMO_KIND == "wavelet":
    zs = np.arange(0, 2000, 1)
    ys = sig.ricker(2000, 100)

xs = np.zeros_like(zs)

ax.plot(xs, ys, zs, zdir='x')

ax = fig.add_subplot(212, projection='3d')

analytic = sig.hilbert(ys)

ys = np.real(analytic)

xs = np.imag(analytic)

ax.plot(xs, ys, zs, zdir='x')

plt.show()
