import h5py
import numpy as np
import matplotlib.pyplot as plt

# set parameters to specify wanted output
phase = '1'
offset = 100000
amt = 5000

# choose file
f = h5py.File("D:\Workspace\Python\EventDetection\data\clear-2017-06-21T07-51-51.046155T+0200-0028560.hdf5", "r")

# create output
# create x-labels
fig, ax1 = plt.subplots()

color = 'tab:red'

# label x-axes
ax1.set_xlabel('voltage')
ax1.set_ylabel('current', color=color)

ax1.plot(f['voltage1'][offset:offset + amt], f['current1'][offset:offset + amt], color="b")
plt.show()

f.close()
