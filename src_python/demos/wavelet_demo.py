import h5py
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal as sig

# set parameters to specify wanted output
phase = '1'
offset = 0
amt = 5000

DEMO_MODE = "sine"
# DEMO_MODE = "2sines"
# DEMO_MODE = "chirp"
WITH_NOISE = False

# setup x/y-axis
x = np.arange(offset, offset + amt, 1)

data = np.zeros_like(x)

# Set data according to chosen DEMO
if DEMO_MODE == "2sines":
    data = np.sin(x / 100.) + np.sin(2.12 * x / 100.)
if DEMO_MODE == "chirp":
    data = sig.chirp(x / 2000., 1, 5, 50, 'logarithmic')
if DEMO_MODE == "sine":
    data = np.sin(x / 100.)

if WITH_NOISE: # add gaussian noise
    data = data + np.random.normal(0,3,amt)


# calculate wavelet transform
A, B = 1, 1000
widths = np.arange(A, min(amt, B))

cwtmatr = sig.cwt(data, sig.ricker, widths)

# plot wavelet transform
fig, axs = plt.subplots(nrows=2, ncols=1, sharex='all')


axs[0].plot(x, data)

axs[1].imshow(cwtmatr[::][::-1], extent=[offset, offset + amt, A, min(amt, B)], cmap='PRGn', aspect='auto',
              vmax=abs(cwtmatr).max(),
              vmin=-abs(cwtmatr).min())
plt.ylabel("widths")
plt.xlabel("time")

plt.show()