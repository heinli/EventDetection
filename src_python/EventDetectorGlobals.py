import collections

MICROEVENT = 1
SUB_MICROEVENT = 2
MACROEVENT = 3

Event = collections.namedtuple('Event', 'Start_Tick End_Tick Max_Pos Duration Overlap Type Peak_Info')
PeakInfo = collections.namedtuple('PeakInfo', 'Prominence Width_Height')
ChunkResult = collections.namedtuple('ChunkResult', 'Events Summ EMDAmps EMDs')
InputChunk = collections.namedtuple('InputChunk', 'Offset CurrentData')
event_settings = collections.namedtuple('event_settings', 'height prominence width distance rel_height')
