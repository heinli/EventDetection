from EventDetectorGlobals import *
import numpy as np
import scipy.interpolate as ip
import scipy.signal as sig
import sys

N_ITERATIONS = 200
EMD_AMT = 5
USED_EMDs = [0, 1, 2, 3, 4]
X_FOR_SPLINE = None
STANDARD_SETTINGS = event_settings(height=500, prominence=300, width=30, distance=50, rel_height=0.85)


def process_chunk(input_chunk, used_emds=USED_EMDs, emd_amt=EMD_AMT, settings=STANDARD_SETTINGS, graphical=True):
    
    amps, emds = calc_emd_amps(input_chunk, used_emds, emd_amt, graphical)
    summed, found_events = get_events(amps[used_emds], input_chunk.Offset, settings=settings)

    if graphical:
        return ChunkResult(Events=found_events, Summ=summed, EMDAmps=amps, EMDs=emds)
    else:
        return ChunkResult(Events=found_events, Summ=None, EMDAmps=None, EMDs=None)


def reprocess_chunk(input_chunk, chunk_result, offset, settings, used_emds=USED_EMDs, emd_amt=EMD_AMT):
    if max(max(used_emds) + 1, emd_amt) > len(chunk_result.EMDAmps):
        return process_chunk(input_chunk, used_emds, emd_amt)
    summed, found_events = get_events(chunk_result.EMDAmps[used_emds], offset, settings)
    return ChunkResult(Events=found_events, Summ=summed, EMDAmps=chunk_result.EMDAmps, EMDs=chunk_result.EMDs)


def calc_emd_amps(input_chunk, wanted_emds, emd_amt, graphical):
    global X_FOR_SPLINE
    global N_ITERATIONS

    residue = input_chunk.CurrentData
    chunk_size = residue.size
    n_emd = 0
    if graphical:
        n_emd = emd_amt
    else:
        n_emd = max(wanted_emds) + 1

    emd_list = np.zeros((n_emd + 1, chunk_size))
    if X_FOR_SPLINE is None or X_FOR_SPLINE.size != chunk_size:
        X_FOR_SPLINE = np.arange(0, chunk_size, 1)

    for j in range(0, n_emd):  # how many EMDs to be calculated
        emd = residue  # current (temporary) emd is the last iterations residue
        stoppage_memory = None
        for i in range(0, N_ITERATIONS):  # chunk_size of smoothing-iterations to be executed
            # calculate all minimums and maximums
            sys.stdout.write('.')
            sys.stdout.flush()

            max_spline = get_envelope(emd, True)
            min_spline = get_envelope(emd, False)
            # subtract the average of the splines from the current residue to get better approximation of EMD
            avg = (max_spline + min_spline) / 2
            new_emd = emd - avg
            should_stop, stoppage_memory = standard_dev_stoppage(emd, stoppage_memory)
            should_stop = should_stop or threshold_stoppage(min_spline, max_spline, avg)
            emd = new_emd
            if should_stop:
                break

        # print inner counter
        print 'Emd ' + str(j)
        # update residue by removing new EMD from the signal
        residue = residue - emd
        # add new EMD to list
        emd_list[j, :] = emd

    emd_list[n_emd, :] = residue
    # calculate amplitudes and instantaneous frequencies of all EMDs
    amps = np.zeros((n_emd + 1, chunk_size))
    inst_freq_list = np.zeros((n_emd + 1, chunk_size - 1))
    for i in range(len(emd_list)):
        amp, inst_phase, inst_freq = do_hilbert(emd_list[i])
        amps[i, :] = amp
        inst_freq_list[i, :] = inst_freq
    return amps, emd_list


def standard_dev_stoppage(new_emd, old_emd):
    if old_emd is None:
        return False, new_emd
    diff = []
    quotient = []
    if old_emd.size > 6000:
        begin = old_emd.size / 2 - 2500
        end = old_emd.size / 2 + 2500
        diff = old_emd[begin: end] - new_emd[begin: end]
        quotient = np.divide(diff, old_emd[begin: end])
    else:
        diff = old_emd - new_emd
        quotient = np.divide(diff, old_emd)
    
    squared = np.square(quotient)
    standard_deviation = np.sum(squared)
    # print standard_deviation
    return (standard_deviation < 200), new_emd


def threshold_stoppage(min_spline, max_spline, avg):
    a = np.subtract(max_spline, min_spline) / 2
    sigma = np.abs(np.divide(avg, a))
    counter = 0
    masked_one = np.ma.masked_greater(sigma, 0.05)
    masked_two = np.ma.masked_greater(sigma, 0.5)
    return masked_one.count() >= 0.05 * 5000 and masked_two.count() == 5000

# stolen from https://www.gaussianwaves.com/2017/04/extracting-instantaneous-amplitude-phase-frequency-hilbert-transform/
def do_hilbert(input_emd):
    # apply hilbert-transform (add imaginary component to make signal analytic)
    analytic = sig.hilbert(input_emd)
    # calculate amplitude of analytic signal
    amp = np.abs(analytic)
    # calculate instantaneous phase of the analytic signal
    inst_phase = np.unwrap(np.angle(analytic))
    # calculate instantaneous frequency of the analytic signal by differentiation
    inst_freq = (np.diff(inst_phase) / (2.0 * np.pi))*250  # *250 to get the frequency in kHz
    return amp, inst_phase, inst_freq


def get_envelope(residue, top):
    global X_FOR_SPLINE
    extrema = []
    if top:  # looking for maximums
        for k in range(1, residue.size - 1):
            if residue[k - 1] < residue[k] and residue[k] > residue[k + 1]:
                extrema.append(k)
    else:  # looking for minimums
        for k in range(1, residue.size - 1):
            if residue[k - 1] > residue[k] and residue[k] < residue[k + 1]:
                extrema.append(k)
    
    y_values = residue[extrema]
    extrema.append(extrema[0] + X_FOR_SPLINE.size)
    y_values = np.append(y_values, y_values[0])
    
    spline = ip.CubicSpline(extrema, y_values, bc_type='periodic')(X_FOR_SPLINE)
    return spline


def get_events(amps, offset,
               settings=STANDARD_SETTINGS):
    summed = np.sum(amps, axis=0)
    smoothed = summed  # smooth(summed,window_len=9,window='flat')
    smoothed = smoothed[0:summed.size]
    peaks, properties = sig.find_peaks(smoothed, height=settings.height, prominence=settings.prominence,
                                       width=settings.width, distance=settings.distance, rel_height=settings.rel_height)

    found_events = []
    previous_event = None

    for i, peak in enumerate(peaks):
        overlap = False
        left = int(properties["left_ips"][i]) 
        right = int(properties["right_ips"][i]) 
        duration = right - left
        
        if i > 0 and i < len(peaks) - 1:
            previous_left = int(properties["left_ips"][i - 1])
            previous_right = int(properties["right_ips"][i - 1])
            next_left = int(properties["left_ips"][i + 1])
            next_right = int(properties["right_ips"][i + 1])
            previous_duration = previous_right - previous_left
            next_duration = next_right - next_left
            
            if left < previous_right or right > next_left:
                overlap = True
            if left < next_left and right > next_right:
                overlap = True
            if left < previous_left and right > previous_right:
                overlap = True
            
        current_event = None
        pInfo = PeakInfo(Prominence=properties["prominences"][i], Width_Height=properties["width_heights"][i])
        if overlap:
            current_event = Event(Start_Tick=left + offset, End_Tick=right + offset, Max_Pos=peak + offset, Duration=right - left, Overlap=overlap, Type=SUB_MICROEVENT, Peak_Info=pInfo)
        else:
            current_event = Event(Start_Tick=left + offset, End_Tick=right + offset, Max_Pos=peak + offset, Duration=right - left, Overlap=overlap, Type=MICROEVENT, Peak_Info=pInfo)
            
        found_events.append(current_event)
        if previous_event is not None and previous_event.Overlap == True and current_event.Overlap == True and current_event.Start_Tick - previous_event.End_Tick < settings.distance:
            combi_event_left = min(previous_event.Start_Tick, current_event.Start_Tick)
            combi_event_right = max(previous_event.End_Tick, current_event.End_Tick)
            found_events.append(Event(Start_Tick=combi_event_left, End_Tick=combi_event_right, Max_Pos=0, Duration=combi_event_right - combi_event_left, Overlap=True, Type=MICROEVENT, Peak_Info=None))
            
        previous_event = current_event
        
    for i in range(0, len(found_events)):
        if i < len(found_events) - 1:
            if found_events[i].Overlap and found_events[i + 1].Overlap and found_events[i].Type == SUB_MICROEVENT and found_events[i + 1].Type == SUB_MICROEVENT:
                currentStart = found_events[i].Start_Tick
                currentEnd = found_events[i].End_Tick
                otherStart = found_events[i + 1].Start_Tick
                otherEnd = found_events[i + 1].End_Tick
                        
                if found_events[i].Duration > found_events[i + 1].Duration:
                    found_events[i] = found_events[i]._replace(Start_Tick=min(currentStart, otherStart))
                    found_events[i] = found_events[i]._replace(End_Tick=otherStart)
                    found_events[i + 1] = found_events[i + 1]._replace(Start_Tick=otherStart)
                    found_events[i + 1] = found_events[i + 1]._replace(End_Tick=max(currentEnd, otherEnd))
                else:
                    found_events[i] = found_events[i]._replace(Start_Tick=min(currentStart, otherStart))
                    found_events[i] = found_events[i]._replace(End_Tick=currentEnd)
                    found_events[i + 1] = found_events[i + 1]._replace(Start_Tick=currentEnd)
                    found_events[i + 1] = found_events[i + 1]._replace(End_Tick=max(currentEnd, otherEnd))
        
    return smoothed, found_events
