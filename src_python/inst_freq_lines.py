import h5py
import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as ip
import scipy.signal as sig

# set parameters to specify wanted output
signal = 'current'
phase = '1'
offset = 3000
amt = 5000
reduction_factor = 1

display_mode = "distorted"  # set to "" if you want to see the actual frequency-wave
emdamt = 0
used_imfs = [0, 1, 2, 3, 4]
timestep = 4 / 1000.
ncols = 1000  # dimensions for heat-map
nrows = 160

# choose file
f = h5py.File("D:\Workspace\Python\EventDetection\data\clear-2017-06-28T15-29-49.319945T+0200-0033803.hdf5", "r")

# read data to operate on
odata = f[signal + phase][offset * reduction_factor:(offset + amt) * reduction_factor:reduction_factor]
residue = f[signal + phase][offset * reduction_factor:(offset + amt) * reduction_factor:reduction_factor]
x = np.arange(0, amt, 1)

imfs = []

for j in range(0, max(max(used_imfs) + 1, emdamt)):  # how many EMDs to be calculated
    imf = residue  # current (temporary) emd is the last iterations residue
    for i in range(0, 40):  # amt of smoothing-iterations to be executed
        # calculate all minima and maxima
        maxima = []
        minima = []

        for k in range(1, amt - 1):
            if imf[k - 1] < imf[k] and imf[k] > imf[k + 1]:
                maxima.append(k)
            if imf[k - 1] > imf[k] and imf[k] < imf[k + 1]:
                minima.append(k)

        # get all amplitudes of all maxima/minma (add first extreme again to make periodic splines possible)
        ymax = list(map(lambda x: imf[x], maxima))
        maxima.append(maxima[0] + amt)
        ymax.append(ymax[0])

        ymin = list(map(lambda x: imf[x], minima))
        minima.append(minima[0] + amt)
        ymin.append(ymin[0])

        # interpolate splines through maxima/minima
        spmax = ip.CubicSpline(maxima, ymax, bc_type='periodic')(x)
        spmin = ip.CubicSpline(minima, ymin, bc_type='periodic')(x)
        # subtract the average of the splines from the current residue to get better appxoximation of EMD
        imf = list(map(lambda q: imf[q] - (spmax[q] + spmin[q]) / 2, range(0, amt)))
    # update residue by removing new EMD from the signal
    residue = residue - imf
    # add new EMD to list
    imfs.append(imf)

# calculate amplitudes and instantaneous frequencies of all EMDs
amps = []
inst_freqs = []
for imf in imfs:
    # apply hilbert-transform (add imaginary component to make signal analytic)
    analytic = sig.hilbert(imf)
    # calculate amplitude of analytic signal
    amp = np.abs(analytic)
    # calculate instantaneous phase of the analytic signal
    inst_phase = np.unwrap(np.angle(analytic))
    # calculate instantaneous frequency of the analytic signal by differentiation
    inst_freq = (np.diff(inst_phase) / (2.0 * np.pi))

    amps.append(amp)
    inst_freqs.append(inst_freq)

# specify heat map dimensions (too big and it can't be displayed by normal screens)
grid = np.zeros((nrows, ncols), dtype=np.float)
dx = amt / (ncols - 1)
dy = (max(map(max, inst_freqs)) - min(map(min, inst_freqs))) / (nrows - 1)

# plot chosen EMDs in heat map
i = 0
for amp, inst_freq in zip(amps, inst_freqs):
    if i in used_imfs:
        idx = np.round((x[1:] - x[1:].min()) / dx).astype(np.int)
        idy = np.round((inst_freq - inst_freq.min()) / dy).astype(np.int)
        grid[np.array(idy), np.array(idx)] += np.array(amp[1:])
        # grid[0, np.array(idx)] += np.array(amp[1:])
    i += 1

# plotting
x = np.arange(offset * timestep, timestep * (offset + amt * reduction_factor), timestep * reduction_factor)

# plot original signal
fig = plt.figure()
ax1 = fig.add_subplot(211)
ax1.set_xlabel('time (ms)')
ax1.set_ylabel(signal + phase)

ax1.plot(x, f[signal + phase][offset:offset + amt * reduction_factor:reduction_factor], zorder=0)

ax2 = fig.add_subplot(212)

# choose color scheme for heat map
# plt.hot()
# plt.flag()
# plt.prism()
if display_mode == "distorted":
    im = ax2.imshow(np.log(grid), cmap=plt.get_cmap('binary'))
else:
    plt.cool()
    im = ax2.imshow(grid)

ax1.legend()
ax1.tick_params(axis='y')
ax1.autoscale(tight=True, axis='x')

plt.show()

f.close()
