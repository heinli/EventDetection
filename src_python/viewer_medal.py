import h5py
import numpy as np
import matplotlib.pyplot as plt

# choose file
f = h5py.File("D:\Workspace\Python\EventDetection\data\medal-8-2017-02-24T14-22-06.270426T+0100-0014129.hdf5", "r")

# set parameters to specify wanted output
phase = '1'
offset = 0
amt = 2000
reduction_factor = 1

v_to_be_graphed = [
    #   'voltage' + '1',
    #   'voltage' + '2',
    #   'voltage' + '3'
]

i_to_be_graphed = [
    'current' + '1',
    # 'current' + '2',
    # 'current' + '3'
]

# constants
TIMESTEP = (1. / 6400)

# create output
# create x-labels
x = np.arange(offset * TIMESTEP, offset * TIMESTEP + amt * reduction_factor * TIMESTEP, reduction_factor * TIMESTEP)
fig, ax1 = plt.subplots()

color = 'tab:red'

# label x-axes
ax1.set_xlabel(u"time (s)")
ax1.set_ylabel('voltage', color=color)
ax1.tick_params(axis='y')

# plot voltages
for v in v_to_be_graphed:
    ax1.plot(x, f[v][offset:offset + amt * reduction_factor:reduction_factor], label=v)

ax1.legend()

ax2 = ax1.twinx()

color = 'tab:blue'
ax2.set_ylabel('current', color=color)
ax1.tick_params(axis='y')

# plot currents
for i in i_to_be_graphed:
    ax2.plot(x, f[i][offset:offset + amt * reduction_factor:reduction_factor], label=i)

fig.tight_layout()
ax2.legend()
plt.show()

f.close()
