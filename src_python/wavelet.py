import h5py
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal as sig

# set parameters to specify wanted output
phase = '1'
offset = 0
amt = 150000

# DEMO_MODE = "medal"
DEMO_MODE = "clear"

# choose file
if DEMO_MODE == "medal":
    f = h5py.File("D:\Workspace\Python\EventDetection\data\medal-8-2017-02-24T14-22-06.270426T+0100-0014129.hdf5", "r")
if DEMO_MODE == "clear":
    f = h5py.File("D:\Workspace\Python\EventDetection\data\clear-2017-06-28T15-29-49.319945T+0200-0033803.hdf5", "r")

x = np.arange(offset, offset + amt, 1)

odata = f['current' + phase][offset:offset + amt]

if DEMO_MODE == "clear":
    b, a = sig.butter(5, 0.03, btype='high', analog=False)
    data = sig.filtfilt(b, a, odata)
else:
    data = odata

# calculate and display wavelet transform
if DEMO_MODE == "clear":
    A, B = 1, 10
if DEMO_MODE == "medal":
    A, B = 1, 200
widths = np.arange(A, min(amt, B))

cwtmatr = sig.cwt(data, sig.ricker, widths)

fig, axs = plt.subplots(nrows=3, ncols=1, sharex='all')

axs[0].plot(x, odata)
axs[0].plot(x, data)

axs[1].imshow(cwtmatr[::-1], extent=[offset, offset + amt, A, min(amt, B)], cmap='PRGn', aspect='auto',
              vmax=abs(cwtmatr).max(),
              vmin=-abs(cwtmatr).min())

s = np.sum(cwtmatr, axis=0)
axs[2].plot(x, 20 * np.log10(np.abs(s)))

plt.show()

f.close()
