\section{Testing on real-world data}
\label{sec:testing}
In this section all the previously mentioned tools for exploring datasets are applied to CLEAR measurements of the BLOND-250 dataset.
And while the main focus will be placed on the event-detecting algorithm based on the HHT, the data is first analyzed using the DFT and the DWT to gain an intuition for the dataset.
\subsection{Preliminary Data Analysis}
\paragraph{DFT}% analyze using DFT => harmonics of the 50Hz base signal very strong up until ~1kHz; peaks around 30kHz and 75kHz (maybe harmonics?)
While applying the DFT to the dataset cannot yield precise information about the events present in the signal, it can still indicate at which frequencies one should look for events.
The spectrum of the current-measurements of the BLOND-250 data can be seen in Figure \ref{fig:spectrum}.
The scale of the diagram has been shifted to place background noise at 0dB.

\begin{figure}
\includegraphics[height=2.2in, width=3.2in]{images/spectrum_annotated.png}
\caption{Spectrum of BLOND data}
\label{fig:spectrum}
\end{figure}

As can be seen in that figure there are three different kinds of peaks to be seen in the spectrum.
The leftmost peak is due to the harmonics of the base frequency of the mains electricity at 50Hz, which can be seen in the frequency range from 0 to 6kHz.
This peak has the most energy, being up to 70dB more powerful than the background noise.

The second kind of peak are broad spectrum peaks, which are can be found at 20-35kHz and at 65-82kHz.
Events corresponding to these peaks would be expected to have a period of 28.6-50$\mu$s and 12.2-15.4$\mu$s, respectively.
Due to these two broad peaks being around a factor of two apart from one another, it can be theorized that they may correspond to harmonics of the same event.

The last kind of event has a very narrow spectrum and can be seen as spikes in the spectrum.
Such peaks can be seen at 13.7kHz, 17.8kHz, 63.4kHz, and 91kHz.
The corresponding periods to the events causing these spikes should have periods of 73$\mu$s, 56$\mu$s, 16$\mu$s, and 11$\mu$s, respectively.

To see what the peaks in the frequency-domain seen in Figure \ref{fig:spectrum} correspond to in the time-domain, we applied band-pass-filters to the original signal, as can be seen in Figure \ref{fig:bandpass}.
\begin{figure}
\includegraphics[height=2.2in, width=3.2in]{images/band_pass.png}
\caption{original signal and band-passed versions of it}
\label{fig:bandpass}
\end{figure}
It can be observed that the micro-events seen in the original signal are probably the cause of most of the peaks in the frequency domain, as most peaks in the band-passed signals tended to coincide with micro-events in the original signal.
And while peaks in the time-domain signals of the broad-spectrum peaks usually coincided with micro-events, the signals of narrow band peaks tended to be more noisy, slightly fluctuating even when the original signal was nearly constant.


\paragraph{DWT}% analyze using DWT => small events in a signal can be seen, but the peaks are very noisy
Unlike the DFT, the DWT yields more information about the location of events at the cost of reducing knowledge about the frequency of those events.
This can be observed in Figure \ref{fig:wavelet_py}.

\begin{figure}
\includegraphics[height=2.2in, width=3.2in]{images/dwt_original.png}
\caption[Short Title]{Output of the wavelet.py-program for the BLOND dataset \par \small
Graph 1: original data (blue); data after high-pass-filtering (orange)\\
Graph 2: scalogram of data after high-pass filtering\\ (abscissa: number of sample; ordinate: width of wavelet)\\
Graph 3: Sum of all rows of scalogram on decibel scale}
\label{fig:wavelet_py}
\end{figure}

Anywhere a micro-event occurs, a line corresponding to it can be seen in the scalogram.
The energy of that line can be seen in the third diagram, which displays of the sum of all rows on a decibel scale.
As can be seen in that diagram, most events have around 10dB more energy than the surrounding background-noise, translating into having around about three\footnote{$10dB=20dB\log_{10}x \rightarrow x = 10^{10/20} \simeq 3.16$} times the energy the background-noise has.

\paragraph{Instantaneous frequencies} Analyzing the instantaneous frequencies of IMFs can reveal which of them might correspond to actual events.

\begin{figure}
\includegraphics[height=2.2in, width=3.2in]{images/inst_freqs.png}
\caption[Short Title]{Output of the inst\_freq\_lines.py-program for the BLOND dataset \par \small
upper graph: input signal\\
lower graph distorted spectrogram (lowest frequencies at the top)\\
IMF0 (black), IMF1 (blue), IMF2 (red), IMF3 (yellow)}
\label{fig:instfreqs}
\end{figure}

As can be seen in Figure \ref{fig:instfreqs} the higher the IMF-number, the less variable the instantaneous frequency of the IMF becomes.
It can also be seen, that the average instantaneous frequency of the IMF will get lower as the IMF-number increases.
This means that to detect micro-events, one has to primarily use the lowest-numbered IMFs, because micro-events tend to have a high frequency.
But one also has to use IMFs, whose frequencies are not too noisy, meaning that their frequencies lie within a narrow band -- a criterion, which is only fulfilled by IMFs with high numbers.
Combining both criteria, it makes sense to only use IMFs 1 and 2 in the detection of events, as they feature both high, and fairly narrow-banded frequencies.


% smoothly transition to our event-detector (those same events are reliably detected by our algorithm based off of the HHT)
Summed up, this preliminary analysis of the data indicates three facts.
Firstly, according to the DFT-analysis we can expect the periods of the base-waves making up the events to be around 10$\mu$s to 75$\mu$s.
Secondly, according to the DWT-analysis we know that the energy of the micro-events is around three times higher than its surrounding background noise.
Thirdly, when using the HHT to analyze the signals for micro-events, the first and second IMF should be used.


\subsection{Analysis of Micro-Bursts}
Now we apply a more advanced method, the HHT, in the form of the implemented event-detector to the data. 
In Figure \ref{graph_results1} an example of the detection can be seen. In the upper graph the sum of amplitudes of the chosen IMFs is displayed.
In the lower graph the boundaries of the detected peaks are used to mark the detected events in the original data.
As can be seen in the upper graph, the amplitude of the sum of IMFs can be up to 10 times higher than their surrounding background noise.
Comparing this result to the preliminary result of the DWT, this means that the HHT not only extracts the events from the original signal, but also achieves a signal-to-noise ratio that is about a factor of 3 higher than it originally was.

Thanks to this, the prototype can detect the position and length of events with sufficient reliability.
However the user has to define what actually classifies as an event.
For example the last detected event in Figure \ref{graph_results1} might not be considered a real event because it is too short.
This fine-tuning can be done by adjusting the parameters mentioned in chapter \ref{impl:parameters}.
When adjusting these parameters, the user should to keep in mind that this modification can also cause some peaks to be suppressed, which actually do correspond to real events.


\begin{figure}
\includegraphics[height=2.2in, width=3.2in]{images/results1.png}
\caption{Input to find\_peaks and detected events}
\label{graph_results1}
\end{figure}

For the results that will be discussed in the next passages the used parameters are shown in Table \ref{table:used:parameters}.
As for why we have chosen to only include IMF1 and IMF2 in the sum used as the input to the peak-detection-algorithm, the following items should be mentioned:
\begin{itemize}
\item When including high numbered IMFs in the sum, the peaks tend to get very broad, up to the point where entire periods of the mains-power-frequency will be considered an event. 
\item Just ignoring the high IMFs is not enough, as the lowest IMF (IMF0) contains a lot of noise, which leads to a noisy input to the peak-detection, resulting in more false positives. 
\end{itemize}
It turns out that using IMF1 and IMF2 for further processing is a good compromise.
Unsurprisingly, this coincides with the IMFs which seemed most promising during the preliminary analysis.
Incidentally, the choice of not including/calculating higher IMFs also has a positive impact on processing speed.

\begin{table}
  \caption{Empirically chosen Parameters}
  \label{table:used:parameters}
  \begin{tabular}{cl}
    \toprule
    Parameter& Value\\
    \midrule
      	IMF0 & NO\\
  	IMF1 & YES\\  	
  	IMF2 & YES\\
  	IMF3 and > 3 & NO\\
  \midrule
    height &  300\\
    prominence & 400\\
    width &  20\\
    distance &  10\\
    rel\_height &   0.8\\
  	\bottomrule
\end{tabular}
\end{table}



\subsection{Results for the BLOND dataset}
As the micro-events considered in this paper have not been analyzed before and also the BLOND dataset is not labeled with respect to those, it is not possible to evaluate the actual performance of the event-detection implemented.
But it is possible to provide some empirical results gained when applying the algorithm to sample files from the dataset. 
For this section five samples were considered. 

Those are: \\
Sample 1: \scalebox{.8}[1.0]{(\texttt{clear-2017-06-21T06-07-20.122303T+0200-0028508.hdf5})}\\
Sample 2: \scalebox{.8}[1.0]{(\texttt{clear-2017-06-21T07-51-51.046155T+0200-0028560.hdf5})}\\
Sample 3: \scalebox{.8}[1.0]{(\texttt{clear-2017-06-21T13-01-22.636113T+0200-0028714.hdf5})}\\
Sample 4: \scalebox{.8}[1.0]{(\texttt{clear-2017-05-16T11-39-20.114921T+0200-0002881.hdf5})}\\
Sample 5: \scalebox{.8}[1.0]{(\texttt{clear-2017-06-03T16-34-36.724819T+0200-0015924.hdf5})}\\
Sample 1 to 3 were chosen because they cover different times of day.
Sample 1 covers 2 minutes of measurement in the early morning where power-consumption is not altered by activity of the researchers in the building.
Sample 2 was chosen to cover two minutes, in which the power-consumption changed, due to someone switching on a device. 
This was done by looking at the daily summary provided by BLOND.
Sample 3 was recorded on the same day at 13 o'clock, when power-consumption is high.
Sample 4 and 5 were additionally chosen randomly to compare the results of sample 1 to 3 with results from a different day. 
\begin{figure}
\includegraphics[height=2.2in, width=3.2in]{images/Eventrate.PNG}
\caption{Eventrate in events/100ms}
\label{fig:eventrate}
\end{figure}

\begin{figure}
\includegraphics[height=2.2in, width=3.2in]{images/Histogram.PNG}
\caption{Event-histogram based on event-duration}
\label{fig:eventhistogram}
\end{figure}

\begin{figure}
\includegraphics[height=2.2in, width=3.2in]{images/Histogram_intra_cycle.PNG}
\caption{Event-histogram based on intra-cycle location}
\label{fig:eventhistogram2}
\end{figure}

Figure \ref{fig:eventrate} shows the average event-rate in events per 100ms in those five files. 
Although it seems reasonable to expect more events during busy office hours the comparison of the first three samples shows no such behavior.
Also the two samples from different days differ only slightly. 

Figure \ref{fig:eventhistogram} shows the histogram of the event-duration.    
Here a slight difference is noticeable. During the day time there seem to be slightly more short events with a duration of around 180 \textmu s than during the night time. 
This also applies to sample 4, which contains data from 11:39 of another day. 
However there exists no obvious explanation for these differences.
In all samples most events have a duration of around 300 \textmu s.
There are no events that are shorter than 80 \textmu s. 
This is because those are rejected by the peak-detection, which is set to a minimal peak-width of 20 samples and one sample covers 4 \textmu s.

A very interesting behavior shows up when considering the position of the events relative to the 50Hz voltage cycle.
It is clearly visible that the distribution is far from random.
In each cycle there exist two periods lasting around 4ms where no events show up.
This behavior is constant throughout the day, and also (Sample 4 and 5) on two completely different days.
Clearly the process inducing the events into the measurements is coupled to the mains frequency in some way.

For these plots no sub-events were considered. 
In case of a peak that was split (indicating it could be two events interfering which each other) only the combined peak was used to generate the histogram.
Also for all plots only phase 3 of the CLEAR data was considered. 


\subsection{Effects of Reduced Sampling-Frequency}
In this section one important question is investigated: Can the method developed here be applied to data with lower sampling frequency?
This is important because to evaluate the stability of the method, it would be beneficial to test it on a different dataset (for example the UK-DALE\cite{UKDALE}).
However each of those datasets uses a different, usually lower sampling frequency.

To answer this question the BLOND-250 data is used, but it is downsampled by powers of 2. 
To do this, the mean of \(2^n\) consecutive values is taken and used as value in the downsampled series.
By using \(n=[1;2;3;4]\) four new series can be acquired having sampling frequencies of \(f_s=[125,000;62,500;31,250;15,625]\). 
Now the resulting data and the results of the event-detection can be inspected empirically.
However the parameters for the event-detection (see \ref{impl:parameters}) must be adjusted manually. 

Yet again the Nyquist-Shannon sampling theorem plays an important role.
It states that by using the sampled signal it is not possible to extract meaningful information for effects at frequencies above \(f_s /2\).

One example of how the parameters need to be changed because of the different sampling frequencies, is the choice of IMFs used for peak-detection.
While the first IMF calculated mostly contained noise of higher frequency than the events at $f_s=250$kHz, it begins capturing the frequency of the events as the sampling frequency is reduced. 
%, meaning that the event-detector works best, when not using IMF0 as input to the peak-detection.
%When this signal is now downsampled, the frequency of the micro-events lies within the highest frequencies still contained in the signal. 
Therefore, for \(f_s=[125,000;62,500]\), the settings for the event-detector must be adjusted to include IMF 0 and 1 rather than 1 and 2.
When further downsampling the signal (\(f_s<=31,250\)) only IMF 0 contains information about the events visible in the original data.


\begin{figure*}
\includegraphics[height=4in, width=7in]{images/Downsampling2.png}
\caption[Caption for LOF]{Event-detector with signals at different sampling-frequency; their corresponding spectra are displayed on the left\textsuperscript{*}}
\small\textsuperscript{*}the spectra are not accurate, and do not take into account aliasing
\label{fig:downsampling}
\end{figure*}

The results of this test are shown in Figure \ref{fig:downsampling}.
It can be observed that a high sampling frequency is beneficial for detecting micro-events in current measurements.
However a sampling frequency below 250kHz might still be sufficient.
Both performance of the algorithm and visual inspection of events in general, begin to quickly degrade at sampling frequencies below 62.5kHz.
The main reason for this lies not in the HHT based method itself, but in the fact that micro-events have a certain frequency $f_{\mu e}$, thereby putting a lower bound on the sampling frequency: \(f_s>2\cdot f_{\mu e}\).

\subsection{Runtime Considerations}
Unfortunately the EMD-algorithm is computationally expensive. 
But it may be possible to improve the runtime if the bottlenecks of this process are found.
Some measurements\footnote{All measurements were performed on a laptop equipped with an Intel Core i5 CPU (i5-3337U CPU @ 1.80GHz) with 4GB of RAM} are therefore documented in this section.

First some theoretical considerations: 
\begin{itemize}
\item The runtime of the algorithm should increase linearly with the number of input data-points
\item The runtime of the algorithm should increase linearly with the number of IMFs to be calculated (in a setting, where the algorithm does not continue until the monotonic residue is found)
\item The number of sifting steps should be constant with respect to the number of input data-points
\end{itemize}

Especially the third point should be emphasized.
When implementing the EMD-algorithm naively and choosing a fixed threshold for the stoppage criteria (see section \ref{emd:stoppage}) the sifting process will depend on the length of the input, due to the definition of the stoppage criteria not factoring out the length of the input.
This problem was solved by only calculating the standard deviation for a cutout of fixed length of the IMFs about to be created.

\paragraph{Evaluation with synthetic settings}
To improve intuition for the behavior of the EMD-algorithm more IMFs than necessary for peak-detection were calculated in the following test.
The algorithm was set to produce 9 IMFs and input sizes of 250,000, 500,000 and 1,000,000 data-points (1s, 2s, 4s). Only one core of the CPU was used.

\begin{table}[]
\caption{Runtime measurements (in s) with synthetic settings}  
\label{table:runtime1}
\begin{tabular}{l c @{\hspace{0.8\tabcolsep}} c @{\hspace{0.8\tabcolsep}} c c @{\hspace{0.8\tabcolsep}} c @{\hspace{0.8\tabcolsep}} c c @{\hspace{0.8\tabcolsep}} c @{\hspace{0.8\tabcolsep}} c}
\toprule
 size & \multicolumn{3}{c}{250,000} & \multicolumn{3}{c}{500,000}  &\multicolumn{3}{c}{1,000,000} \\
& \(t_\text{IMF}\) &\(n_\text{sift}\)  &\(t/n_\text{sift}\)  &  \(t_\text{IMF}\) &\(n_\text{sift}\)  &\(t/n_\text{sift}\) &   \(t_\text{IMF}\) &\(n_\text{sift}\)  &\(t/n_\text{sift}\)  \\
\midrule
 IMF0& 12.1 & 23 & 0.53 & 20.7 & 18 & 1.15 & 34.4 &14  & 2.46 \\
 IMF1 & 11.9 &  26& 0.46 & 49.9 & 51 & 0.98 & 65.1 & 31 & 2.10 \\
 IMF2& 15.1 & 36 & 0.42 & 27.8 &  30& 0.93 & 45.1 & 23 & 1.96 \\
 IMF3&  15.0& 37 & 0.41 & 22.3 & 25 & 0.89 & 51.7 & 28 & 1.85 \\
 IMF4&  14.1& 35 & 0.40 & 31.0 & 36 & 0.86 & 71.9 & 35 & 2.05 \\
 IMF5&  16.8& 43 & 0.39 & 29.5 & 35 & 0.84 & 52.4 & 30 & 1.75 \\
 IMF6& 14.4 & 36 & 0.40 & 31.0 & 37 & 0.84 & 79.9 & 47 & 1.70\\
 IMF7& 22.0 & 57 & 0.39 & 123.0 & 146 & 0.84 & 96.5 & 57 & 1.69\\
 IMF8& 18.9 & 49 & 0.39 & 39.1 & 46 & 0.85 & 97.7 & 58 & 1.68\\
 \midrule
 Total&  140.3&  &  & 374.2 &  &  & 594.6 &  & \\
 Mean  & &38 &0.42 & &47.1 & 0.91& & 35.9&1.92 \\
 \bottomrule
 
\end{tabular}
\end{table}

Table \ref{table:runtime1} shows the results of this test. 
Firstly, it can be seen that number of sifting steps (\(n_\text{sift}\)) performed until the stoppage criteria is reached can not be predicted exactly, as this process is completely data-dependent.
For example for IMF7 in the second test 146 steps of sifting were performed, which also has a large influence on the total runtime.
Maybe small perturbations in the input data in this example led to bad behavior of the cubic spline estimation.
However the number of sifting steps also does not increase by a factor of two when using the longer input data, showcasing that the problem of calculating the standard deviation was indeed solved.

The processing time per step of sifting (\(t/n_\text{sift}\)) is reasonably constant holding the input-size constant.
A slight decrease as later IMF are processed is noticeable.
The reason for this is that later IMFs contain fewer minima and maxima.
With respect to the size of the input, the processing time increases almost linearly. 
This can be seen by computing the factor between the mean of (\(t/n_\text{sift}\)) for input-size 250,000 and 500,000 for example.
This factor turns out to be 2.17.
In going from input-size 500,000 to 1,000,000 the factor is 2.11.
So as expected the computation time for one step of sifting increases almost linearly (\(\mathcal{O}(n_{input})\)).

The total processing time for the entire algorithm is dependent on the number of IMFs to calculate, the processing time for one step of sifting (dependent on the size of the input) and the steps of sifting until the stoppage criterion is reached. 
As this last parameter has high fluctuations it can also happen that if the size of the input is doubled, the total runtime increases by a factor less than 2, which can be seen by comparing the values 374.2s and 593.6s from the table.

\paragraph{Evaluation with practical settings}
For this test, the event-detection was configured to only use IMF 1 and 2. 
So in total three IMFs had to be generated.
As it is not possible to process the complete data in one pass, because most machines do not have sufficient RAM to do so, the algorithm is implemented to divide the data into smaller chunks.
Here a chunk-size of 500,000 (2s) was used.
Measurements were taken for input-sizes of 2,000,000 (8s) and 4,000,000 (16s).
Also the benefits of using multiple cores were explored.


\begin{table}[]
\caption{Runtime measurements (in s) with practical settings}  
\label{table:runtime2}
\begin{tabular}{l c  c  c c}
\toprule
 input-size & \multicolumn{2}{c}{2,000,000} & \multicolumn{2}{c}{4,000,000} \\
& EMD & Total  & EMD  & Total  \\
\midrule
 Sequential & 402.7 & 406.9 & 783.7 & 791.7  \\
 Parallel (4 Cores) &  & 177.3 &  & 347.5  \\
 \bottomrule
\end{tabular}
\end{table}

Table \ref{table:runtime2} shows the results, from which it can be seen that the EMD-algorithm takes the most time of the overall process.
The Hilbert-Transform and the usage of \texttt{find\_peaks()} takes less than 1\% of the total time.
By extrapolating to a full 120s sample of the BLOND-250 dataset the following statements about the runtime can be made:
\begin{itemize}
\item In sequential mode a full 120s sample should take around 100 minutes to process.
\item In parallel mode with 4 cores a full 120s sample should take around 40 minutes to process.
\end{itemize}

 