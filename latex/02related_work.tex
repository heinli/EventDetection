\section{Methods}
In this section three common methods, namely the Discrete Fourier-, the Discrete Wavelet-, and the Hilbert-Huang-Transform, are first briefly explained, to then show their advantages and disadvantages through examples.

\subsection{Discrete Fourier Transform (DFT)}
Generally speaking the DFT is a transformation of a time domain signal into the frequency domain.
%, where a signal of frequency $f$ in the frequency-domain translates into a sine wave $s[k]=sin(2\pi fk)$ in the time-domain, where $s[k]$ is the $k$th sample of that signal.
This is done by interpreting the space of all possible complex signals $S = \mathbb{C}^{N+1}$ of length $N+1$ as a vector space, and defining the dot product between two elements of this vector space $a,b \in S$ as $a\cdot b=\sum_{k=0}^{N}a[k]\overline{b[k]}$.
It turns out that using this definition of the dot product, the following set of signals forms an orthonormal basis of $S$.
\[
B:=\{b_n[k]=\exp({\frac{2\pi i}{N+1}kn}) | n\in \{0;..;N\}\}
\]
Using this basis, the DFT is nothing more than a transformation from the natural basis to the basis $B$.
The coefficient $x_n$ of the base-vector $b_n$ can then be interpreted as the prevalence of the frequency $\frac{n}{N+1}$ in the signal $s\in S$.
As the coefficients $x_n$ can be complex-valued (even if $s\in \mathbb{R}$) they are usually not directly displayed, but instead $20\log_{10}\left|x_n\right|$dB is used for that purpose.

Having understood that the Fourier Transform is nothing but a basis change, the coefficient $x_n$ of the base-vector $b_n$ for a signal $a[k]$ can be calculated using:\footnote{the factor $-1$ in the exponent is introduced due to the definition of the dot-product}
\[
x_n = \sum_{k=0}^Na[k]\cdot \exp({\frac{-2\pi i}{N+1}kn})
\]

When applying the DFT to actual signals two parameters have to be chosen.
The first one is the sampling frequency $f_s$ and the second one is the number of used samples $N+1$.
Both parameters influence the set of frequencies, which will be analyzed when applying the DFT.
The lowest analyzed frequency, at $n=0$, is always $0$, i.e. a constant signal.
The highest frequency, according to the Nyquist-Shannon sampling theorem, will be $\frac{f_s}{2}$, which when equating with  $\frac{n}{N+1}$ yields $n = \frac{f_s\cdot (N+1)}{2}$.
As all remaining analyzed frequencies linearly interpolate between the lowest and highest frequency, the final set of analyzed frequencies is:
\[
F=\{f_n=\frac{f_sn}{N+1}~|~n\in\{0,...,\frac{N+1}{2}\}\}
\]

\begin{figure}
\includegraphics[height=3.0in, width=3.2in]{images/exampleDFT1.png}
\caption{Input signal and its spectrum}
\label{exampleDFT1}
\end{figure}
\begin{figure}
\includegraphics[height=3.0in, width=3.2in]{images/exampleDFT2.png}
\caption{Input signal and its spectrum}
\label{exampleDFT2}
\end{figure}

\paragraph{Example:}
Figure \ref{exampleDFT1} and Figure \ref{exampleDFT2} show two input signals and their corresponding spectra.
Both input signals consist of two sine waves of differing frequency.
The first input signal is the sum of the two sine waves over an entire period.
The second input signal consists of just the first sine for the first half of the observed time frame, and just the second sine for the second half.
Such a signal is called \textit{non-stationary}.
As can be seen both input signals produce a similar amplitude in frequency space, even though they are different in time space.
This is due to the basis-vectors (complex sine and cosine waves) having a constant amplitude in time-space, meaning that a single sine-wave has the same amplitude at the start of the analyzed time-frame as at the end.
This means that a strong 'match' with a base-sine-wave at frequency $f$ for just the first half of the observed time-frame, will be seen the same way as a weaker 'match' with the same sine-wave throughout the entire observed time-frame.

Of course all basis changes -- including the DFT -- are reversible.
Therefore, even if two different signals have identical amplitude spectra, the results of the DFTs are reversible.
The information missing in those similar amplitude spectra, shown in Figures \ref{exampleDFT1} and \ref{exampleDFT2}, to reverse the transform, is contained within the phase spectrum, which contains the phases of the coefficients $x_n$.

It can thus be said that the main disadvantage of the DFT lies within the high difficulty of detecting when a certain frequency was observed, as this would require interpretation of the phase spectrum of a signal.
In other words the DFT is not suitable for analyzing non-stationary signals.
Unfortunately most signals acquired from real word physical processes will have at least some non-stationary content.
The largest advantage of the DFT is its computational efficiency, at $\mathcal{O}(n\log n)$.

%For one the sampling frequency $f_s$ can be chosen.
%This parameter dictates the maximum frequency $f_m$ which can be extracted using the DFT through the Nyquist-Shannon sampling theorem, which states: $f_s>2f_m$.
%The second parameter is the number of samples $N+1$ used for calculating the DFT.
%This parameter influences the resolution with which the frequencies present in the current-signal are analyzed.
%For every two extra samples the number of analyzed frequencies goes up by one.

\subsection{Discrete Wavelet Transform (DWT)}
Like the DFT, the DWT uses a transformation to an orthonormal basis to draw conclusions about an input signal.
In the case of the DWT this new basis consists of so called wavelets, which can be transformed into one another by dilating and translating them.
Usually one so-called mother-wavelet $\psi$ is used to construct all base-wavelets by dilating and translating the mother-wavelet.
This way a wavelet can be dilated by a factor $w$ and translated by $\tau$ in the following way, to generate the wavelet $\psi_{w,\tau}(t)$\cite{shark2006design}.
\[
\psi_{w,\tau}(t)=\psi(\frac{t-\tau}{w})/\sqrt{w}
\]
To guarantee that these base-wavelets form a valid base, the mother-wavelet has to fulfill two conditions:

1. $\sum_{t=-\infty}^{\infty}\psi(t)=0$

2. $\sum_{t=-\infty}^{\infty}[\psi(t)]^2=1$


Another key feature of wavelets is that their features are localized\cite{shark2006design}, i.e. the wavelets are almost always zero except for one short period of time in which they exhibit a characteristic signal-shape.

This time-restricted nature has the effect that a basis-coefficient $x_{w,\tau}$ for a wavelet $\psi_{w,\tau}$ after a transformation of a signal $s\in S$ not only yields information concerning the frequency $f\backsim 1/w$ of $s$, but also about the location $\tau$ within $s$ of where this frequency is prevalent.
It should be noted that when using the DWT not the frequency, but its inverse, the period, is implicitly calculated.
This is because one cannot directly choose the frequency $f$ of any given wavelet.
Instead the width of the wavelet $w\backsim 1/f$ is chosen.
This is also the reason why the ordinate of wavelet-spectrograms is usually a width and not a frequency.

When applying the DWT to signal measurements, three parameters have to be set.
Firstly, the range of wavelet widths can be chosen, which implicitly changes the range of analyzed frequencies.
Secondly, the number of samples used for the DWT can be chosen.
But unlike the DFT this does not directly influence the highest analyzable frequency.
It only gives an upper bound to the range of wavelet-widths, as matching a wavelet longer than the basis-signal would make little sense.
The third parameter is the sampling frequency $f_s$, which directly correlates to the frequencies corresponding to the wavelets.
This means that a wavelet of width $w=20$ would correspond to lower frequencies at $f_s=f_{s1}$, than it would at $f_s=2f_{s1}$.

\begin{figure}
\includegraphics[height=2.2in, width=3.5in]{images/exampleDWT.png}
\caption{A chirp and its corresponding DWT-spectrogram}
\label{exampleDWT}
\end{figure}
\paragraph{Example:}
When applying the DWT to a chirp, as can be seen in Fig. \ref{exampleDWT}, one can see that the the width of the matched wavelet goes down over time, as was to be expected, because the frequency is increasing.
One can also observe the main disadvantage of the DWT, namely its inability to determine the instantaneous frequency of the signal at every point in time.
Instead the instantaneous frequency and energy of a signal can be determined only once per cycle of said signal.
This discreteness is the main weakness of the DWT.
On the bright side, the DWT is capable of detecting a change in frequency over time and does so at a reasonable computational complexity.

\subsection{Hilbert-Huang-Transform (HHT)}
The HHT, first described by Huang et al. \cite{huang1998empirical}, takes a different approach than the previous two methods.
Instead of dictating what the orthogonal basis vectors should look like, it empirically generates its own set of orthogonal basis vectors from a given base-signal, using the so-called Empirical Mode Decomposition (EMD).
The resulting basis vectors are then called Intrinsic Mode Functions (IMFs).
Thereafter the Hilbert-Transform is applied to the IMFs, the result of which is then used to analyze the instantaneous energy and frequency of the IMFs  -- and thus the signal -- at any point in time.
Contrary to the other methods explained so far, the HHT was explicitly developed to deal with \textit{non-stationary} and \textit{non-linear} signals.

The two steps of the HHT will be explained in detail in the next sections.
