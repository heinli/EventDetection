import h5py
import numpy as np
import matplotlib.pyplot as plt
from scipy import signal as sig

# choose file
f = h5py.File("D:\Workspace\Python\EventDetection\data\clear-2017-06-21T07-51-51.046155T+0200-0028560.hdf5", "r")

# set parameters to specify wanted output
phase = '1'
offset = 50000
amt = 5000
reduction_factor = 1

# constants
TIMESTEP = 4 / 1000.

# create output
# create x-labels
#x = np.arange(offset * TIMESTEP, (offset + amt * reduction_factor) * TIMESTEP,
#              reduction_factor * TIMESTEP)
x = np.arange(offset,offset+amt,1)
fig, axs = plt.subplots(nrows=6, ncols=1, sharex='all')


# plot currents
ORIGINAL_DATA_COMPLETE = f['current1'][offset:offset + amt:1]
b, a = sig.butter(5, 0.09, btype='high', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b, a, ORIGINAL_DATA_COMPLETE)
b, a = sig.butter(20, 0.16, btype='low', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b,a,ORIGINAL_DATA_COMPLETE)
axs[0].plot(x,ORIGINAL_DATA_COMPLETE)
axs[0].set_xlabel("13.7 & 17.8kHz")


ORIGINAL_DATA_COMPLETE = f['current1'][offset:offset + amt:1]
b, a = sig.butter(15, 0.16, btype='high', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b, a, ORIGINAL_DATA_COMPLETE)
b, a = sig.butter(20, 0.30, btype='low', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b,a,ORIGINAL_DATA_COMPLETE)
axs[1].plot(x,ORIGINAL_DATA_COMPLETE)
axs[1].set_xlabel("20-35kHz")


ORIGINAL_DATA_COMPLETE = f['current1'][offset:offset + amt:1]
b, a = sig.butter(20, 0.45, btype='high', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b, a, ORIGINAL_DATA_COMPLETE)
b, a = sig.butter(20, 0.53, btype='low', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b,a,ORIGINAL_DATA_COMPLETE)
axs[2].plot(x,ORIGINAL_DATA_COMPLETE)
axs[2].set_xlabel("63.4kHz")


ORIGINAL_DATA_COMPLETE = f['current1'][offset:offset + amt:1]
b, a = sig.butter(30, 0.53, btype='high', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b, a, ORIGINAL_DATA_COMPLETE)
b, a = sig.butter(20, 0.7, btype='low', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b,a,ORIGINAL_DATA_COMPLETE)
axs[3].plot(x,ORIGINAL_DATA_COMPLETE)
axs[3].set_xlabel("65-82kHz")


ORIGINAL_DATA_COMPLETE = f['current1'][offset:offset + amt:1]
b, a = sig.butter(15, 0.70, btype='high', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b, a, ORIGINAL_DATA_COMPLETE)
b, a = sig.butter(5, 0.80, btype='low', analog=False)
ORIGINAL_DATA_COMPLETE = sig.filtfilt(b,a,ORIGINAL_DATA_COMPLETE)
axs[4].plot(x,ORIGINAL_DATA_COMPLETE)
axs[4].set_xlabel("91kHz")


ORIGINAL_DATA_COMPLETE = f['current1'][offset:offset + amt:1]
axs[5].plot(x,ORIGINAL_DATA_COMPLETE)
axs[5].set_xlabel("original data")

fig.tight_layout()
plt.show()

f.close()
