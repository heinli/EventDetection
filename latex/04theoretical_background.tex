
\subsubsection{Empirical Mode Decomposition}
The first step in the two-step Hilbert-Huang transform is the Empirical Mode Decomposition (EMD).
It splits the given signal into so called intrinsic mode functions (IMFs). 

\paragraph{IMF}
Any signal is an intrinsic mode function if it satisfies two conditions:
\begin{itemize}
\item The number of extrema and the number of zero crossings must either be equal or differ at most by one
\item The local mean value of the upper envelope (defined by the local maxima) and the lower envelope (defined by the local minima) must be zero.
\end{itemize}
A graphical counterexample for those conditions is shown in Figure \ref{not_a_imf}.
In the first example in the region of x=1900 there are local maxima with value below zero.
For those maxima and minima the zero crossings are missing and so the total number of zero crossings will not match the number of extrema.
The second counterexample seems to have enough zero crossings, but the upper envelope has a greater magnitude than the lower envelope.
Instead of a mean-value of around zero, the mean value of upper and lower envelope in the region x=1500 to x=2500 will  be around 0.1.
The third graphic shows a proper IMF.

\begin{figure}
\includegraphics[height=2.2in, width=3.2in]{images/Not_a_IMF.JPG}
\caption{Counterexamples for IMF conditions}
\label{not_a_imf}
\end{figure}

If the two conditions are satisfied, it is ensured that at every point in time an instantaneous frequency of the signal can be defined and thus also be computed by applying the Hilbert Transform.

\paragraph{The EMD algorithm}
As data acquired from real world measurements are unlikely to fulfill the IMF-conditions, they they need to be transformed first.
%For data acquired from real world (physical) processes it is very unlikely that the data fulfills the IMF-conditions.
%Therefore the data has to be transformed first.
This can be achieved by applying the EMD algorithm.
The algorithm, however, does not transform the data into a single IMF, but instead splits it into several IMFs.
This corresponds to separating physical effects on different time-scales.


\begin{algorithm}
\caption{EMD algorithm}\label{emd_algo}
\begin{algorithmic}[1]
\Procedure {EMD}{$X(t)$}
\State $r \leftarrow X(t)$ 
\State $k \leftarrow 0$
\While{$r$ is not monotonic}
\State $i \leftarrow 0$
\State  $h_{ki} \leftarrow r$
\While{$h_{ki}$ not fulfilling IMF-conditions}
\State $maxima \leftarrow$ findMaxima($h_{ki}$)
\State $minima \leftarrow$ findMinima($h_{ki}$)
\State $upperEnvelope \leftarrow$ cubicSpline($maxima$)
\State $lowerEnvelope \leftarrow$ cubicSpline($minima$)
\State $m_{ki} \leftarrow (upperEnvelope + lowerEnvelope) / 2$
\State $h_{k(i+1)} \leftarrow h_{ki} - m_{ki}$ 
\State $i \leftarrow i + 1$
\EndWhile
\State $c_k \leftarrow h_{ki}$
\State $k \leftarrow k + 1$
\State $r \leftarrow r - h_{ki}$
\EndWhile
\State
\Return ${[c_0, c_1, ... , c_n, r]}$
\EndProcedure

\end{algorithmic}
\end{algorithm}

The algorithm begins with the input data. 
After the first IMF has been constructed, it is subtracted from the input and the algorithm continues to produce the next IMF from the result of this subtraction.
The first IMF contains information about the highest-frequency parts of the measured process (often the first IMF mostly contains noise).
The following IMFs contain information about effects on larger timescales.
This continues until the result of the aforementioned subtraction contains no more local extrema, but instead is a monotonic function.
This is called the \emph{residue}.
If the \emph{residue} is not just a constant, it can reveal a global trend in the data.

In a mathematical formulation we denote the input signal as \(X(t)\), the $k$th IMF as \(c_k\) and the final residue as \(r\).
The decomposition can then be written as \[X(t)=\sum_{k=1}^n c_k+r\]


The construction of IMFs is carried out by an iterative process called \textit{sifting}.
An intermediate IMF that still needs more steps of sifting is denoted as \(h_{ki}\), where $k$ is the IMF about to be constructed and $i$ is the number of sifting iterations already performed.
In the same fashion the mean of the upper and the lower envelope of the signal is denoted as \(m_{ki}\). 
One step of sifting can be written as:
\[h_{k(i+1)}=h_{ki}-m_{ki}\]

For a computer scientist it might be more easy to understand this process if written down in algorithmic formulation (algorithm \ref{emd_algo}).
One step of sifting happens in lines 7 - 12.
First all the local maxima and minima have to be identified.
Then an upper and a lower envelope is created by fitting a cubic spline through all the maxima and all the minima.
The mean of the upper and lower envelope is subtracted from the input data, which, in the first iteration, should center the signal roughly around zero.
The sifting process is repeated until the aforementioned IMF conditions are met.

So in total, the EMD algorithm consists of an outer loop, which produces one IMF per iteration and an inner loop, in which multiple iterations of sifting are performed. 




\paragraph{Stoppage criteria}
\label{emd:stoppage}
An important decision that has to be made is when to stop the sifting process.
Up until now the answer to that question is to stop when a data-series is produced, that meets the IMF conditions (see line 6 in algorithm \ref{emd_algo}).
In practice this is not easy to compute, because the second condition (mean value of upper and lower envelope must be zero) will never be fulfilled due to numerical instabilities of calculations with floating-point numbers.
Also doing too many steps of sifting could obliterate amplitude fluctuations that carry important information about the underlying process \cite{huang1998empirical}. 

A more practical stoppage criteria could be that the mean of the envelopes should be close to zero, but using a fixed threshold for this condition ignores the different amplitudes the input signal might have over time.
A better approach is to leverage the fact, that sifting is a converging process, i.e. in each iteration of sifting the changes made to the data should be smaller than in the previous iteration.
Therefore Huang et al. propose the standard deviation between the results of two consecutive sifting steps as stoppage criterion:
\[SD_k=\sum_{t=0}^{T}\frac{|h_{k(i-1)}(t)-h_{ki}(t)|^2}{h_{k(i-1)}^2 (t)}\]
Sifting stops if the value calculated for SD is below a predefined threshold.
This value is dependent on the length of the input data \(T\).
So a threshold valid for processing short chunks of data may be invalid when processing longer chunks.

The standard deviation however is not the only possible stoppage criterion\cite{Huang2317}.
For example the S-number criterion can also be used: It stops the sifting process when the number of consecutive sifting steps, in which the number of zero-crossings and extrema does not change, reaches a predefined threshold.
Yet another criterion is the threshold method\cite{rilling2003empirical}.

\subsubsection{Hilbert Transform}
The second step in the two-step HHT is to take the IMFs calculated in the first step and extract their instantaneous amplitude and frequency.

To achieve this, the HHT takes the approach of first expanding the IMFs to the complex number plane in such a way that they become analytic, i.e. that their "negative frequencies" become zero \cite{MDFT07}.
For this the Hilbert Transform is applied, whose aim it is to take in a signal $x[k]$ and put out a signal $y[k]$, such that the signal $x[k]+iy[k]$ is analytic.
It turns out that the transformation, which achieves this, is a convolution with the function $\frac{1}{\pi t}$, which means that the Hilbert Transform can be written as:
\[H(u)(t)=(\frac{1}{\pi t}*u)(t)=\frac{1}{\pi}\int_{-\infty}^{\infty}\frac{u(\tau)}{t-\tau}d\tau\]
As this is an operation that is supposed to affect the frequency domain, it makes sense to view it in the frequency domain by applying the Fourier Transform.
According to the convolution theorem the Fourier Transform of a convolution of two functions, is equal to the product of their Fourier Transforms.
It can thus be shown that\cite{zayed1996handbook}: 
\begin{align*}
{\mathcal {F}}(H(u))(\omega )
&={\mathcal {F}}(\frac{1}{\pi t}*u)(\omega )\\
&={\mathcal {F}}(\frac{1}{\pi t})(\omega )\cdot\mathcal {F}(u)(\omega )\\
&=(-i\operatorname {sgn}(\omega ))\cdot {\mathcal {F}}(u)(\omega ) 
\end{align*}
where $\operatorname{sgn}(\cdot)$ is the function that returns the sign of its input.
Now observing the Fourier Transform of the transformed signal $u(t)+iH(u)(t)=x[k]+iy[k]$ yields:
\begin{align*}
\mathcal {F}(u+iH(u))(\omega) &= \mathcal {F}(u)(\omega )+i(-i\operatorname {sgn}(\omega ))\cdot {\mathcal {F}}(u)(\omega )\\
&=\mathcal {F}(u)(\omega )+\operatorname {sgn}(\omega )\cdot {\mathcal {F}}(u)(\omega )
\end{align*}
This means that if the input frequency $\omega$ is negative, the result of the Fourier Transform is zero.
And if the input frequency $\omega$ is positive, the result of the Fourier Transform is twice that of the Fourier Transform of the original signal.
This factor of two ensures that the result of the transformation $x[k]+iy[k]$ still has the same amount of energy as the original signal $x[k]$, by taking the energy of the removed "negative frequencies" and adding it to the corresponding "positive frequency" of same absolute value.
The reason the energy at frequency $-\omega$ can just be added to the energy at frequency $\omega$, is that for real-valued inputs to the Fourier Transform such as $u$, the following identity holds true\cite{steeb2013hilbert}: 
\[
\overline{\mathcal {F} (u)(-\omega)}= \mathcal {F} (u)(\omega) \rightarrow \left|\mathcal {F} (u)(-\omega)\right|= \left|\mathcal {F} (u)(\omega)\right|
\]

According to Huang et al. \cite{huang1998empirical} the result $X_j(t)$ of the sum of the $j$th IMF $x_j(t)$ and its Hilbert Transformation $iH(x_j)(t)$  can be written as:
\begin{align*}
X_j(t) &= x_j(t)+iH(x_j)(t)\\
&=  a_j(t)\exp (i\phi_j) = a_j(t)\exp (i\int \omega_j(t)dt)
\end{align*}
Thus the envelope or instantaneous amplitude/energy  $a_j(t)$ of the IMF $x_j(t)$ can be calculated as $a_j(t)=\left|X_j(t)\right|$.
On top of this the instantaneous phase $\phi_j(t)$ of $X_j(t)$ can be calculated by computing:
\[
\phi_j(t)=\arg(X_j(t))=\arctan\frac{\Im(X_j(t))}{\Re(X_j(t))}
\]
From this one can then also calculate the instantaneous frequency of the signal by numerically approximating ${\omega_j(t)=\frac{d\phi_j(t)}{dt}}$.