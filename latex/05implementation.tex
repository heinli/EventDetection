\section{Implementation}\label{sec:implementation}
In this section we will go into detail as to how the implementation of the previously explained methods can be accessed, and how that implementation is structured.
% what language
All prototypes were implemented in the programming language Python (more specifically Python 2), and are built to read files of the \texttt{.hdf5} format.
Most of them use matplotlib-library to display their results graphically.
The interested reader can find the code of these prototypes via Git at:\\ 
\texttt{https://gitlab.lrz.de/heinli/EventDetection}.

The prototypes are broadly clustered into tools and demos.
While the demos can be used to gain intuition about the methods previously described, the tools can be used to explore certain features in a data set.
The source code for the tools can be found in the \texttt{src\_python} folder.
The demos can be found within the \texttt{src\_python/demos} folder.

\paragraph{Tools}
The tools include file viewers for both MEDAL- and CLEAR-files (\texttt{viewer\_medal} and \texttt{viewer\_clear}), a tool for analyzing the frequency-spectrum of data using the DFT (\texttt{spectrumViewer}), a tool for applying the wavelet transform to datasets (\texttt{wavelet}), a prototype for applying the Hilbert-Huang-Transform to extract the instantaneous frequencies (\texttt{inst\_freq\_lines}), and programs for applying the Hilbert-Huang-Transform and an event-detector based on it to a given dataset (\texttt{EventDetector*}).


While the file viewers only read in the data from the \texttt{.hdf5}-files and then display them, the remaining tools apply transformations to the data.

The tool for analyzing the frequency-spectrum of measurements, simply applies the Fast-Fourier-Transform to the signal and then displays the result after converting to decibel.
The user can also specify whether or not to filter out the low frequencies below 3kHz before applying the transformation.

The tool for applying the wavelet transform has two modes, which are optimized for either macro- or micro-events, depending on whether the data that is being read is a MEDAL- or a CLEAR-file.
The used mother-wavelet is the Ricker-Wavelet which is sometimes also referred to as the Mexican hat wavelet.
In case of the MEDAL-file wavelet-widths between 1 and 200 are applied.
In case of the CLEAR-file we first apply a high-pass filter to remove the harmonics of the mains frequency (50Hz) up to 3kHz, and only thereafter do we apply the wavelets-widths between 1 and 10.

In both cases the result of the DWT is firstly plotted as a scalogram, and secondly as a time-signal-energy graph.
This second graph displays the sum of of all rows of the scalogram, on a decibel-scale.

The prototype for viewing the instantaneous frequencies applies the Hilbert-Huang-Transform to its input signal, and thereafter displays the instantaneous frequencies $\omega_j(t)$ in a scalogram.
In the scalogram the abscissa displays time and the ordinate displays frequency.
For every pair of values $(\omega_j(t);a_j(t))$ retrieved from applying the Hilbert Transform to the IMFs, the value $a_j(t)$ is be added to the dot in the diagram representing frequency $\omega_j(t)$ at time $t$.
This prototype can be useful when analyzing which IMFs correspond to physical events captured by a measurement-signal.
%In the context of NILM, such IMFs usually display a continuous, but non-constant curve in the scalogram, i.e. its graph is not scattered to look like noise, but also not always at the same level, indicating that its frequency does not change over time.

\paragraph{HHT-based Event-Detector}
The last and most complex tool first applies the Hilbert-Huang-Transform to an input signal, and then detects events in that signal by analyzing the output of the HHT.
To detect the events the algorithm uses the sum of instantaneous amplitudes of some chosen IMFs to generate a signal-energy-curve.
On this signal-energy-curve a standard peak-detection-algorithm is applied.
To optimize the parameters of which IMFs to use in the creation of the signal-energy-curve, and the parameters of the peak-detection-algorithm, the user can utilize the GUI, found in \texttt{EventDetectorGUI}.
And while the GUI is not optimized for speed, it is still fast enough for its application, as the user only needs to view a small sample (5000 measurements) of an entire file (3,000,000 measurements) to optimize the parameters for it.
Then, as soon as the optimal parameters for detecting events in a specific dataset have been found, they can be applied to an entire file at once, using the program in file \texttt{EventDetectorScript}.
This program has been optimized for performance using multithreading.
The EventDetectorScript can be directed to analyze a certain hdf5-file of CLEAR measurements by providing the path to the file on the command-line.
After the file has been processed, the program creates a CSV-file with the results.
In this file one event per line is represented by: 
\begin{enumerate}
\item Number of found event in this CLEAR-file
\item Start sample
\item End sample
\item Event duration in number of samples
\item Overlap: a boolean value describing if this event was considered to consist of two overlapping events by the peak-detection. This applies also to the artificially created event encompassing the two overlapping event
\item Event-Type: '1' if the is a single event (not overlapping) or was artificially constructed out of overlapping events. '2' otherwise.
\end{enumerate} 
The filename will be the input-filename plus the extension \texttt{Detected Events.csv}.
An example call is \texttt{python EventDetectorScript.py path/to/data/data-file.hdf5}.
Multiple files can be processed by simply writing multiple such calls into a command-line script.


Both of the GUI and the script use the same logic in the background, which has been outsourced to the files \texttt{EventDetectorGlobals} and \texttt{EventDetectorHHT} to avoid code duplication.



\paragraph{Parameters of the Event-Detector}
\label{impl:parameters}
The parameters that influence the event-detection algorithm can either be provided via the GUI or, when\texttt{EventDetectorHHT} is used in the context of another custom program, by providing settings to the function call \texttt{process\_ chunk()}.
 
The most interesting parameter, which deals directly with the Hilbert-Huang-Transform, is the choice of IMFs, whose instantaneous amplitudes will be added together.
This is important, because the next step in the program only uses this sum to detect events in the original signal.

\begin{table}
  \caption{Parameters of Peak-Detection}
  \label{table-parameters}
  \begin{tabular}{cp{6.3cm}}
    \toprule
    Parameter&Description\\
    \midrule
    height &  Peaks smaller than this value are ignored.\\
    prominence & Prominence is a relative parameter, that determines that a peak has to be larger that its neighboring peaks. It can be used to suppress small peaks that lie on the shoulder of larger peaks.\\
    width &  Peaks narrower than this value are ignored.\\
    distance &  The minimal distance between two peaks. If two peaks are too close, the smaller one will be rejected.\\
    rel\_height &  Used to define at which height of the peak the width is measured. 1.0 means measure the width at the absolute lowest point, which will lead to very broad event-borders.\\
  \bottomrule
\end{tabular}
\end{table}

All remaining parameters affect the peak-detection-algorithm.
Their description is provided in table \ref{table-parameters}.
As a standard method from the Python package \texttt{scipy} is used for peak-detection, the description of those parameters may also be looked up in its documentation \cite{FindPeaks}.




%\paragraph{Performance:}
%In this section the performance of the implementations of the DFT, the DWT, and the high-pass filter that is used in both of these applications, is discussed.


% description of programs
% 	demos
%	wavelet-transform (sine-wave, two sine waves at once, and a chirp; all with optional white noise)
% 	demo, which shows what the hilbert-transform does to certain input-signals

% data science tools 
%	file-viewers for both MEDAL- and CLEAR-files
%	spectrum-viewer, which can be used to 
%	plot a clear-signal and the instantaneous frequencies of its IMFs in a diagram
%	wavelet-transform applied to a CLEAR-signal and to a MEDAL-signal

%	HHT: GUI, Script, Globals, Program


% structure of demos: first section after imports is section where parameters can be changed
% standard parameters: 
%	amt: number of samples to be used in the program;
%	offset: offset of first read sample within file;
%	reduction_factor: integer factor by which sampling frequency should be reduced;
%	signal (current or voltage): what kind signal should be read
%	phase: which of the current-phases should be displayed
%	parameters specific to program
%	file from which to read

